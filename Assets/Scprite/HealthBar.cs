using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class HealthBar : MonoBehaviour
{
   // public PlayerHealth Health;
    public Slider healbar;
    public float myHealth;
   

    // Start is called before the first frame update
    void Start()
    {

        healbar.minValue = 0;
        healbar.maxValue = PlayerHealth.MaxHealth;
    }

    // Update is called once per frame
    void Update()
    {
         SetHealth();
          
        healbar.value = myHealth;
    }

    public void SetHealth()
    {
        myHealth = PlayerHealth.Health;
    }
    
}

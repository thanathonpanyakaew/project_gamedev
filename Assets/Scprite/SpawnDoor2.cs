using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace Scprite
{
    public class SpawnDoor2 : MonoBehaviour
    {
        [SerializeField] int goal = 15;
        public GameObject Door_prefab;
        public static int countMon = 0;
        public Text AlienNum_text;
        // Update is called once per frame

        private void Awake()
        {
            countMon = 0;
        }
        private void Update()
        {
            if (countMon == goal)
            {

                Instantiate(Door_prefab);
                Door_prefab.transform.position = this.transform.position;
                countMon = 0;
                AlienNum_text.enabled = false;
            }
            AlienNum_text.text = "AlienLeft : " + (goal - countMon);
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Scprite.Player;

namespace Scprite
{
    public class AnmoPlayer : MonoBehaviour
    {
        public Text score_text;

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            score_text.text = "Anmo  " + PlayerController.mag_anmo + "/" + PlayerController.total_anmo;
        }
    }
}



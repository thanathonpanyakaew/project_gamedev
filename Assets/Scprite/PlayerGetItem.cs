using System;
using System.Collections;
using System.Collections.Generic;
using Scprite.Player;
using UnityEngine;

public class PlayerGetItem : MonoBehaviour
{
    public int Heal;
    public AudioClip getItemSound;
  

    public void OnCollisionEnter(Collision other)
    {

        if (gameObject.tag == "Player")
        {

            if (other.gameObject.tag == "COIN")
            {
                GetComponent<AudioSource>().PlayOneShot(getItemSound);
                GetCoin._getcoin += 1;
                Score.Myscore += 20;
                Destroy(other.gameObject);
            }

            if (other.gameObject.tag == "HEAL")
            {
                GetComponent<AudioSource>().PlayOneShot(getItemSound);
                PlayerHealth.Health += Heal;
                Score.Myscore += 10;
                Destroy(other.gameObject);
            }

            if (other.gameObject.tag == "ARMOR")
            {
                GetComponent<AudioSource>().PlayOneShot(getItemSound);
                //เพิ่มเกราะ
                ItemArmor.m_armor = true;
                Debug.Log(ItemArmor.m_armor);


                // Invoke("GetArmor",2f);
                Score.Myscore += 10;
                Destroy(other.gameObject);
            }

            if (other.gameObject.tag == "BULLET")
            {
                GetComponent<AudioSource>().PlayOneShot(getItemSound);
                //ไปฟังชั่นเพิ่มทิศทางกระสุน

                //เพิ่มspeed bullet

                //เพิ่มกระสุน
                PlayerController.total_anmo += 20;

                Score.Myscore += 10;
                Destroy(other.gameObject);
            }
            //เก็บปืน
            if (other.gameObject.tag == "Shotgun")
            {
                GetComponent<AudioSource>().PlayOneShot(getItemSound);
                //change gun
                PlayerController.weapon1 = false;
                PlayerController.weapon2 = true;
                PlayerController.weapon3 = false;
                //change anmo
                PlayerController.total_anmo = 60;
                PlayerController.mag_size = 30;
                PlayerController.mag_anmo = 30;

                PlayerGun._bulletSpeed = 1.0f;

                Destroy(other.gameObject);
            }
            if (other.gameObject.tag == "MachineGun")
            {
                GetComponent<AudioSource>().PlayOneShot(getItemSound);
                //change gun
                PlayerController.weapon1 = false;
                PlayerController.weapon2 = false;
                PlayerController.weapon3 = true;
                //change anmo
                PlayerController.total_anmo = 30;
                PlayerController.mag_size = 5;
                PlayerController.mag_anmo = 5;

                PlayerGun._bulletSpeed = 0.5f;

                Destroy(other.gameObject);
            }
        }

    }

   



}

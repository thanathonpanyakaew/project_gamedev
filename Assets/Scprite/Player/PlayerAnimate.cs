using Scprite.Player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
public class PlayerAnimate : PlayerController
{
    Animator animator; 

  
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        Keyboard keyboard = Keyboard.current;
        //Move
        if (keyboard[m_TurnLeftKey].isPressed) { animator.SetBool("IsMoving", true); }
        else if (keyboard[m_TurnRightKey].isPressed) { animator.SetBool("IsMoving", true); }
        else if (keyboard[m_ForwardKey].isPressed) { animator.SetBool("IsMoving", true); }
        else if (keyboard[m_BackwardKey].isPressed) { animator.SetBool("IsMoving", true); }
        else animator.SetBool("IsMoving", false);
        //Shoot
        if (Input.GetButton("Fire1")) animator.SetBool("IsShoot", true);
        else animator.SetBool("IsShoot", false);
        //Die

        //Dash

    }
}

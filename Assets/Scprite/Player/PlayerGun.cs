using UnityEngine;
using Scprite.Player;

namespace Scprite.Player
{
    public class PlayerGun : MonoBehaviour
    {
        [SerializeField]  Transform _firePoint;
        [SerializeField]  GameObject _bulletPrefab;
        [SerializeField] float _bulletDelay;
        public static float _bulletSpeed;
        public AudioClip shotSound;

        private float _LastTimeShot = 0;

        public static PlayerGun Instance;

        void Awake()
        {
            Instance = GetComponent<PlayerGun>();
        }
        void Start()
        {
            _bulletSpeed = _bulletDelay;
        }
        void Update()
        {
            if (PlayerController.weapon1 == true)
            {
                _bulletPrefab.tag = "Bullet";
            }
            if (PlayerController.weapon2 == true)
            {
                _bulletPrefab.tag = "Bullet2";
            }
            if (PlayerController.weapon3 == true)
            {
                _bulletPrefab.tag = "Bullet3";
            }
        }

        public void Fire()
        {
            if (_LastTimeShot + _bulletSpeed <= Time.time && PlayerController.mag_anmo > 0)
            {
                _LastTimeShot = Time.time;
                Instantiate(_bulletPrefab, _firePoint.position, _firePoint.rotation);
                PlayerController.mag_anmo--;
                GetComponent<AudioSource>().PlayOneShot(shotSound);
            }
        }
    }
}
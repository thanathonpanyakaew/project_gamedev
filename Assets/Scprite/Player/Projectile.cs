﻿using System;
using UnityEngine;

namespace Scprite.Player
{
    public class Projectile : MonoBehaviour
    {
        private Vector3 firingPoint;
        [SerializeField] private float projectileSpeed;
        [SerializeField] private float MaxProjectileDistance;
        [SerializeField] private float DurationBullet;

        private void Start()
        {
            firingPoint = transform.position;
        }

        private void Update()
        {
            MoveProjectile();
        }

        void MoveProjectile()
        {
            if (Vector3.Distance(firingPoint, transform.position) > MaxProjectileDistance)
            {
                Destroy(this.gameObject);
            }
            else
            {
                transform.Translate(Vector3.forward * projectileSpeed * Time.deltaTime);
            }
            Destroy(this.gameObject,DurationBullet);
        }
    }
}
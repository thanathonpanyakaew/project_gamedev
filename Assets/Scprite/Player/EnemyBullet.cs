using UnityEngine;
using Debug = System.Diagnostics.Debug;

namespace Scprite.Player
{
    public class EnemyBullet : MonoBehaviour
    {
      
        public static int damage = 2;
        
        private void OnCollisionEnter(Collision target)
        {
            if (target.gameObject.tag == "Player")
            {

                TakeDamage();
            }
            else
            {
                //Objectอื่นๆ
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag == "Player")
            {

                TakeDamage();
            }
            else
            {
                //Objectอื่นๆ
            }
        }

        public static  void TakeDamage()
        {
            PlayerHealth.Health -= damage;
        }
    }
}
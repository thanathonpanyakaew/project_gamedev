
using UnityEngine;
using UnityEngine.InputSystem;


namespace Scprite.Player
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private float MoveSpeed;
        
        [Header("Keys Config")] 
        [SerializeField] protected Key m_ForwardKey = Key.W;
        [SerializeField] protected Key m_BackwardKey = Key.S;
        [SerializeField] protected Key m_TurnLeftKey = Key.A;
        [SerializeField] protected Key m_TurnRightKey = Key.D;

        
        //����ع������
        public static int total_anmo = 50;
        //����ع����ԧ��
        public static int mag_anmo = 15;
        //�ӹǹ����ع������
        public static int mag_size = 15;

        public static bool weapon1 = true;
        public static bool weapon2 = false;
        public static bool weapon3 = false;

        // Start is called before the first frame update
        void Start()
        {
        
        }

        // Update is called once per frame
        void Update()
        {
            MovementInput();
            RotationInput();
            ClickToFire();
            Reload();
            //Debug.Log(total_anmo);
            //Debug.Log(mag_size - mag_anmo);
            //Debug.Log(mag_anmo);

            
            if(weapon1 == false && total_anmo <= 0 && mag_anmo == 0)
            {
                weapon1 = true;
                weapon2 = false;
                weapon3 = false;

                total_anmo = 50;
                mag_anmo = 15;
                mag_size = 15;

                PlayerGun._bulletSpeed = 1.5f;
            }
            
        }
        void MovementInput()
        {
            Keyboard keyboard = Keyboard.current;
            if (keyboard[m_TurnLeftKey].isPressed)
            {
                this.transform.Translate(-MoveSpeed* Time.deltaTime, 0,0);

            }
            else if (keyboard[m_TurnRightKey].isPressed)
            {
                this.transform.Translate(MoveSpeed* Time.deltaTime, 0,0);
            }
            else if (keyboard[m_ForwardKey].isPressed)
            {
                this.transform.Translate(0, 0 , MoveSpeed * Time.deltaTime);
            }
            else if (keyboard[m_BackwardKey].isPressed)
            {
                this.transform.Translate(0, -0,-MoveSpeed * Time.deltaTime);
            }
            
        }

        void RotationInput()
        {
            RaycastHit _hit;
            Ray _ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(_ray, out _hit))
            {
                transform.LookAt(new Vector3(_hit.point.x, transform.position.y, _hit.point.z));
            }
        }

        void ClickToFire()
        {
            if (Input.GetKey(KeyCode.Mouse0))
            {
                PlayerGun.Instance.Fire();
                


            }   
        }

        void Reload()
        {
            if (Input.GetKey(KeyCode.R))
            {
                //����ع���
                if (mag_anmo == mag_size)
                {
                    return;
                }
                //�������ع
                if (total_anmo >= mag_size - mag_anmo)
                {
                    total_anmo -= (mag_size - mag_anmo);
                    mag_anmo = mag_size;

                }
                //�����
                else
                {
                    mag_anmo += total_anmo;
                    total_anmo = 0;
                }
            }
            
        }    
    }
}
using System;
using System.Collections;
using System.Collections.Generic;
using Scprite.Player;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public  class PlayerHealth : MonoBehaviour
{
    public  static  int Health;
    public static int MaxHealth = 100;

    public AudioClip hitReact;
    public AudioClip hitArmerReact;
    public AudioClip enemiesAttack;

    float timeleft = 2;

    // Start is called before the first frame update
    void Start()
    {
        Health = MaxHealth;
    }
    // Update is called once per frame
    void Update()
    {
        timeleft -= Time.deltaTime;
        if (Health <= 0)
        {
            SceneManager.LoadScene("SceneGameOver");
        }
        //Debug.Log(timeleft);       
    }
    public void OnCollisionEnter(Collision target)
    {
        if(ItemArmor.m_armor == false)
        {
            if (target.gameObject.tag == "EnemyBullet")
            {
                Health -= 5;
                GetComponent<AudioSource>().PlayOneShot(hitReact);
                timeleft = 2;
            }
            if (target.gameObject.tag == "Enemy")
            {
                Health -= 5;
                GetComponent<AudioSource>().PlayOneShot(enemiesAttack);
                GetComponent<AudioSource>().PlayOneShot(hitReact);
                timeleft = 2;
            }           
        }
        else if(ItemArmor.m_armor == true)
        {
            if (target.gameObject.tag == "Enemy")
            {
                ItemArmor.m_Armor -= 10;
                GetComponent<AudioSource>().PlayOneShot(enemiesAttack);
                GetComponent<AudioSource>().PlayOneShot(hitArmerReact);
                timeleft = 2;
            }       
            if (target.gameObject.tag == "EnemyBullet")
            {
                ItemArmor.m_Armor -= 5;
                GetComponent<AudioSource>().PlayOneShot(hitArmerReact);
                timeleft = 2;
            }
        }
    }
    public void OnCollisionStay(Collision target)
    {
        if(timeleft <= 0)
        {
            if (ItemArmor.m_armor == false)
            {
                if (target.gameObject.tag == "EnemyBullet")
                {
                    Health -= 5;
                    GetComponent<AudioSource>().PlayOneShot(hitReact);
                    timeleft = 2;
                }
                if (target.gameObject.tag == "Enemy")
                {
                    Health -= 10;
                    GetComponent<AudioSource>().PlayOneShot(enemiesAttack);
                    GetComponent<AudioSource>().PlayOneShot(hitReact);
                    timeleft = 2;
                }
            }
            else if (ItemArmor.m_armor == true)
            {
                if (target.gameObject.tag == "Enemy")
                {
                    ItemArmor.m_Armor -= 10;
                    GetComponent<AudioSource>().PlayOneShot(enemiesAttack);
                    GetComponent<AudioSource>().PlayOneShot(hitArmerReact);
                    timeleft = 2;
                }
                if (target.gameObject.tag == "EnemyBullet")
                {
                    ItemArmor.m_Armor -= 5;
                    GetComponent<AudioSource>().PlayOneShot(hitArmerReact);
                    timeleft = 2;
                }
            }
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ToNextScene : MonoBehaviour
{
    public int NextScene;
    public int Numberscene;

    // Start is called before the first frame update
    void Start()
    {
        NextScene = SceneManager.GetActiveScene().buildIndex + Numberscene;
    }

    private void Update()
    {
        Score.HighScore = Score.Myscore;
    }

    // Update is called once per frame
    private void OnCollisionEnter(Collision target)
    {
        if(target.gameObject.tag == ("Player"))
        {
            SceneManager.LoadScene(NextScene);
        }
          
    }
}

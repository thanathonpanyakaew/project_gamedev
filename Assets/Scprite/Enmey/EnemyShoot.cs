using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Scprite.Enmey;

public class EnemyShoot : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] Transform _firePoint;
    [SerializeField] GameObject _bulletPrefab;

    [SerializeField] float _bulletSpeed;

    public AudioClip enemiesShot;

     float _LastTimeShot = 0;

    GameObject target;
    int hp;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        target = GameObject.FindWithTag("Player");
        transform.LookAt(target.transform);
        if (_LastTimeShot + _bulletSpeed <= Time.time)
        {
            _LastTimeShot = Time.time;
            Instantiate(_bulletPrefab, _firePoint.position, _firePoint.rotation);
            GetComponent<AudioSource>().PlayOneShot(enemiesShot);
        }

        int hp = Boss_Health.MaxHealth;
        if (hp <= hp / 2)
        {
            _LastTimeShot -= 0.5f;
        }
    }
}

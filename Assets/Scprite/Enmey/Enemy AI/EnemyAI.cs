using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;
using UnityEngine.AI;

public class EnemyAI : MonoBehaviour
{
    // Start is called before the first frame update
    [Header("Movement Setting")]
    [SerializeField] private Transform player;
    [SerializeField] private BoxCollider DetectBox;
    private Vector3 TargetDir;
    [SerializeField] private float Speed = 10;
    
    const int IDLE = 0;
    const int CHASE = 1;
    const int DIE = 2;
    static int state;

    [Header("Animation Setting")]
    [SerializeField] private Animator m_Animator;
    [SerializeField] private CurrentState m_currentState;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (state == IDLE)
        {
            IdleState();
            m_Animator.SetBool("isWalk", false);
            
        }
        else if (state == CHASE)
        {
            ChasingState();
            m_Animator.SetBool("isWalk", true);
        }
        
        else if (state == DIE)
        {
            DeadState();
            m_Animator.SetBool("isWalk", false);
            m_Animator.SetBool("isDead", true);
            m_Animator.SetTrigger("playDead");
        }
    }

    protected void ChasingState()
    {
        if (this.transform.position.x > player.position.x)
        {
            this.gameObject.transform.Translate(-Speed * Time.deltaTime, 0, 0);
        }
           
        else if (this.gameObject.transform.position.x < player.position.y)
        {
            this.gameObject.transform.Translate(Speed * Time.deltaTime, 0, 0);
        }

        if (this.gameObject.transform.position.z > player.position.z)
        {
            this.gameObject.transform.Translate(0, 0, Speed * Time.deltaTime);
        }
        else if (this.gameObject.transform.position.z < player.position.z)
        {
            this.gameObject.transform.Translate(0, 0, -Speed * Time.deltaTime);
        }
    }
    
    protected void IdleState()
    {
        
    }

    protected void DeadState()
    {
        
    }

    private void BoxChasing(BoxCollider DetectBox)
    {
        if (DetectBox.isTrigger == true)
        {
            
        }
    }

    enum CurrentState
    {
        Idle,
        Walking,
        Dead
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace Scprite.Enmey.EnemyAI
{
    public class EnemyAI_3 : MonoBehaviour
    {
        public NavMeshAgent agent;
        Transform Player;
        Animator animator;

        private void Awake()
        {
            
        }
        void Start()
        {
            animator = GetComponent<Animator>();
        }
        // Update is called once per frame
        private void Update()
        {
            Player = GameObject.FindWithTag("Player").transform;
            //agent.SetDestination(Player.position);
        }
        
        private void OnTriggerStay(Collider other)
        {
            if (other.gameObject.tag == "Player")
            {
                agent.SetDestination(Player.position);
                animator.SetBool("IsWalk", true);
            }
            
        }
        private void OnTriggerExit(Collider other)
        {
            if (other.gameObject.tag == "Player")
            {
                animator.SetBool("IsWalk", false);
            }
        }
    }
}


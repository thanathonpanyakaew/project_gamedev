using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Scprite.Enmey;

namespace Scprite.Enmey.EnemyAI
{
    public class Boss_AI : MonoBehaviour
    {
        // Start is called before the first frame update

        public float speed = 10;

        public GameObject Boss;
        int pattern = 0;
        int hp;



        void Start()
        {
            

        }

        // Update is called once per frame
        void Update()
        {
            int hp = Boss_Health.MaxHealth;
            


            //pattern
            //doBasicPattern();
            doPattern();
            Debug.Log(hp);
            if (hp <= hp/2)
            {
                speed += 10;
            }
            else if (hp >= hp / 2)
            {
                speed = 10;
            }

            if (pattern >= 3)
            {
                pattern = 0;
            }
        }


        private void doPattern()
        {
            if (pattern == 0)
            {
                this.transform.Translate(speed * Time.deltaTime, 0, 0);
                if (Boss.transform.position.x >= 25)
                {
                    pattern++;
                }
            }
            else if (pattern == 1)
            {
                this.transform.Translate(-speed * Time.deltaTime, 0, 0);
                if (Boss.transform.position.x <= -25)
                    pattern++;
            }
            else if (pattern == 2)
            {
                this.transform.Translate(speed * Time.deltaTime, 0, 0);
                if (Boss.transform.position.x >= 0)
                    pattern++;
            }

            Debug.Log(pattern);
            Debug.Log(Boss.transform.position.x);
        }
    }
}



using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Scprite.Enmey;

public class Boss_AI2 : MonoBehaviour
{
    // Start is called before the first frame update
    public float speed = 10;

    //public GameObject Boss;
    ControlData[] Pattern;
    public float timeCountDown = 5.0f;
    float timeLeft = 5.0f;

    int CurrentIndex = 0;

    void Start()
    {
        timeLeft = timeCountDown;

        Pattern = new ControlData[4];
        for (int i = 0; i < Pattern.Length; i++)
            Pattern[i] = new ControlData();

        Pattern[0].turnRight = 1;
        Pattern[0].turnLeft = 0;
        Pattern[1].turnRight = 0;
        Pattern[1].turnLeft = -1;
        Pattern[2].turnRight = 0;
        Pattern[2].turnLeft = -1;
        Pattern[3].turnRight = 1;
        Pattern[3].turnLeft = 0;
    }

    // Update is called once per frame
    void Update()
    {
        timeLeft -= Time.deltaTime;

        //Debug.Log(timeLeft);
        doBasicPattern();

        int hp = Boss_Health.MaxHealth;
        if (hp <= hp / 2)
        {
            speed += 10;
        }
        else if (hp >= hp / 2)
        {
            speed = 10;
        }

    }

    private void doBasicPattern()
    {
        transform.Translate((Pattern[CurrentIndex].turnRight * speed) * Time.deltaTime, 0, 0);
        transform.Translate((Pattern[CurrentIndex].turnLeft * speed) * Time.deltaTime, 0, 0);

        //Debug.Log(Pattern[CurrentIndex].turnRight);
        if(timeLeft <= 0.0f)
        {
            CurrentIndex++;
            timeLeft = timeCountDown;
        }

        if (CurrentIndex >= Pattern.Length)
            CurrentIndex = 0;
    }

    class ControlData
    {
        public float turnRight;
        public float turnLeft;
        
    }
}

using Unity.VisualScripting;
using UnityEngine;
using Random = UnityEngine.Random;
using Scprite;

namespace Scprite.Enmey
{
    public class EnemyHealth : MonoBehaviour
    {
        
        [SerializeField] private int EnemyMaxHealth = 100;
        
        private int MaxHealth ;
        public GameObject m_Prefab1,m_Prefab2,m_Prefab3,m_Prefab4, m_Prefab5, m_Prefab6;

        public AudioClip dieSound;
        public AudioClip hitReact;

        private int spawnitem;
        
        private GameObject go;

        void Start()
        {
            
            MaxHealth = EnemyMaxHealth;
        }
        void Update()
        {
            if (MaxHealth <= 0)
            {
                GetComponent<AudioSource>().PlayOneShot(dieSound);
                Destroy(this.gameObject );
                Door.countMon++;
                SpawnPrefab();
            }

            if(MaxHealth ==0)
            {
                Score.Myscore += 10;
            }
        }

        public void OnCollisionEnter(Collision target)
        {
            if (target.gameObject.tag == "Bullet")
            {
                MaxHealth -= 50;
                GetComponent<AudioSource>().PlayOneShot(hitReact);
            }

            if (target.gameObject.tag == "Bullet2")
            {
                MaxHealth -= 150;
                GetComponent<AudioSource>().PlayOneShot(hitReact);
            }
            
            if (target.gameObject.tag == "Bullet3")
            {
                MaxHealth -= 300;
                GetComponent<AudioSource>().PlayOneShot(hitReact);
            }
            
        }

        public void SpawnPrefab( )
        {
            spawnitem = Random.Range(1, 12);
            Debug.Log(spawnitem);

            
            switch (spawnitem)
            {
                     case 1 :
                         go = Instantiate( m_Prefab1);
                         go.transform.position =transform.position;
                         break;
                         
                     case 2:
                         break;
                     
                     case 3: 
                         go = Instantiate( m_Prefab2);
                         go.transform.position =transform.position;
                         break;
                     
                     case 4:
                         break;
                         
                     case 5:
                         go = Instantiate( m_Prefab3);
                         go.transform.position =transform.position;
                         break;
                     
                     case 6:
                         break;
                     
                     case        7 :
                         go = Instantiate( m_Prefab4);
                         go.transform.position =transform.position;
                         break;
                     
                     case 8 :
                         break;
                     case        9 :
                         go = Instantiate( m_Prefab5);
                         go.transform.position =transform.position;
                         break;
                     
                     case 10 :
                         break;
                    case        11 :
                         go = Instantiate( m_Prefab6);
                         go.transform.position =transform.position;
                         break;
                     
                     case 12 :
                         break;
                     
            }

           

        }
    }
}
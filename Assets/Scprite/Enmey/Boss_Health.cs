using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Scprite.Enmey
{
    public class Boss_Health : MonoBehaviour
    {
        public int EnemyMaxHealth = 1000;

        public static int MaxHealth = 1000;
        public GameObject m_Prefab1, m_Prefab2, m_Prefab3, m_Prefab4;
        public AudioClip dieSound;
        public AudioClip hitReact;

        private int spawnitem;

        private GameObject go;

        void Start()
        {

            MaxHealth = EnemyMaxHealth;
        }
        void Update()
        {
            if (MaxHealth <= 0)
            {
                GetComponent<AudioSource>().PlayOneShot(dieSound);
                Destroy(this.gameObject);

                SceneManager.LoadScene("SceneWin", LoadSceneMode.Single);
            }
            Debug.Log(MaxHealth);

        }

        public void OnCollisionEnter(Collision target)
        {
            if (target.gameObject.tag == "Bullet")
            {
                MaxHealth -= 50;
                GetComponent<AudioSource>().PlayOneShot(hitReact);
            }

            if (target.gameObject.tag == "Bullet1")
            {
                MaxHealth -= 150;
                GetComponent<AudioSource>().PlayOneShot(hitReact);
            }

            if (target.gameObject.tag == "Bullet2")
            {
                MaxHealth -= 300;
                GetComponent<AudioSource>().PlayOneShot(hitReact);
            }

        }

        public void SpawnPrefab()
        {
            spawnitem = Random.Range(1, 8);
            Debug.Log(spawnitem);


            switch (spawnitem)
            {
                case 1:
                    go = Instantiate(m_Prefab1);
                    go.transform.position = transform.position;
                    break;

                case 2:
                    break;

                case 3:
                    go = Instantiate(m_Prefab2);
                    go.transform.position = transform.position;
                    break;

                case 4:
                    break;

                case 5:
                    go = Instantiate(m_Prefab3);
                    go.transform.position = transform.position;
                    break;

                case 6:
                    break;

                case 7:
                    go = Instantiate(m_Prefab4);
                    go.transform.position = transform.position;
                    break;

                case 8:
                    break;
            }
        }
    }
}


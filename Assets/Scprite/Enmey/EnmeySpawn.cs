using System;
using UnityEngine;

namespace Scprite.Enmey
{
    public class EnmeySpawn : MonoBehaviour
    {
        public GameObject Enmey_Prefab;
        public float timeStart = 10;
        float timeleft = 10;
        

        private void Start()
        {
            timeleft = timeStart;
        }

        private void Update()
        {
            timeleft -= Time.deltaTime;
            if(Boss_Health.MaxHealth >= 0)
            {
                if (timeleft <= 0)
                {
                    spawnEnemy();
                    timeleft = timeStart;
                }
            }
            
            
        }

        private void spawnEnemy()
        {
            GameObject Enmey_Obj = Instantiate(Enmey_Prefab);
            Enmey_Prefab.transform.position = this.gameObject.transform.position;
        }
        
    }
}
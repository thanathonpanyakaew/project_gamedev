using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GetCoin : MonoBehaviour
{
    public static int _getcoin;
    
    [SerializeField ] Text Coin;
    // Start is called before the first frame update
    void Start()
    {
        _getcoin = 0;
        
    }

    // Update is called once per frame
    void Update()
    {
        Coin.text = "Coin : " + _getcoin;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemTypeComponent : MonoBehaviour
{

    [SerializeField] public ItemType m_ItemType;
    // Start is called before the first frame update
    public ItemType Type
    {
        get
        {
            return m_ItemType;
        }
        set
        {
            m_ItemType = value;
        }
    }
}

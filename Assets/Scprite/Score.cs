using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;       

public class Score : MonoBehaviour
{
    
    public static int Myscore;

    public static int HighScore;

    public Text score_text;
    // Start is called before the first frame update
    void Start()
    {
        
        HighScore = PlayerPrefs.GetInt("highscore", HighScore);
    }

    // Update is called once per frame
    void Update()
    {
        if(Myscore > HighScore)
        {
            HighScore = Myscore;
            PlayerPrefs.SetInt("highscore", HighScore);
        }

        HighScore = Myscore;

        score_text.text = "Score : " + Myscore;
    }
    public void ResetScore()
    {
        Myscore = 0;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class CountdownTimer : MonoBehaviour
{

   // public int currentTime = 0;
    public int startingTime = 30;

   [SerializeField ] Text Time;

   public bool takingAway;
    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {

        if (takingAway == false && startingTime >0)
        {
            StartCoroutine(Timetake());
        }

        if (startingTime == 0)
        {
            SceneManager.LoadScene("SceneGameOver");
        }
        

    }

    IEnumerator Timetake()
    {
        takingAway = true;
        yield return new WaitForSeconds(1);
        startingTime -= 1;
        Time.text = "Time : " + startingTime;
        takingAway = false;
        
        
        
    }
}

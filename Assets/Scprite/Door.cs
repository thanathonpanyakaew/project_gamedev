using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Scprite
{
    public class Door : MonoBehaviour
    {
        [SerializeField] int goal = 15;       
        public static int countMon = 0;
        public Text AlienNum_text;
        public static int secene_now = 0;
        // Update is called once per frame

        private void Awake()
        {
            countMon = 0;
        }
        private void Update()
        {
            if (countMon == goal)
            {
                if(secene_now == 0)
                {
                    SceneManager.LoadScene("SceneMap2", LoadSceneMode.Single);
                    secene_now++;
                }
                else if (secene_now == 1)
                {
                    SceneManager.LoadScene("SceneMap3", LoadSceneMode.Single);
                    secene_now--;
                }
            }
            AlienNum_text.text = "AlienLeft : " + (goal - countMon);
        }
    }
}


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GameoverScore : MonoBehaviour
{
    public Text gaemover_score;
    public int score_over;
    // Start is called before the first frame update
    void Start()
    {
        score_over = Score.Myscore;
    }

    // Update is called once per frame
    void Update()
    {
        gaemover_score.text = " " + score_over;
    }
}
